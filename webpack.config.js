const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: "./src/bootstrap.tsx",
    output: {
        filename: "bundle.js",
    },
    mode: "development",
    // Enable sourcemaps for debugging webpack's output.
    devtool: "eval",
    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js"]
    },
    module: {
        rules: [
            // Handle .ts and .tsx file via ts-loader.
            { test: /\.tsx?$/, loader: "ts-loader" }
        ],
    },
    plugins: [
        new CopyPlugin([{
            from: './*.html'
        }])
    ],
};