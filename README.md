# Preact + Webpack + TypeScript

To reduce complexity and the learning curve is no dev server here, but `yarn webpack --watch` does a good job, too.

I personally use the [Caddy webserver](https://caddyserver.com) to serve the app like this: `caddy -root dist`. 
Doesn't have the live-reloading of the webpack-dev-server, but is closer to the way you'd serve your static app on production.

For a solution that can install be installed automatically via yarn, I can recommend `yarn add http-server` and add something like `"serve": "http-server dist"` to the `"scripts"` in `package.json`.